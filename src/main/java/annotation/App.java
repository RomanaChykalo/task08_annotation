package annotation;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class App {
    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException {
        MyClass myClass = new MyClass(15, "Bogdan", false, false);

        new ObjectCognizer(myClass);

    }
}
