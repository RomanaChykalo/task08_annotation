package annotation;

import java.util.ArrayList;
import java.util.List;

public class MyClass {
    @MyAnnotation(age = 18)
    private int age;
    private String name;
    private boolean isStudent;
    private boolean scholarship;

    MyClass(int age, String name, boolean isStudent, boolean scholarship) {
        this.age = age;
        this.name = name;
        this.isStudent = isStudent;
        this.scholarship = scholarship;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public boolean isStudent() {
        return isStudent;
    }

    @AnnotationForMethod(value = 10)
    public void printHello() {
        System.out.println("Persons are waiting for hello");
    }

    public int calculateGraduationYear(int age) {
        int graduatinYear = age + 5;
        return graduatinYear;
    }

    public void hasScholarship(boolean isStudent, boolean scholarship) {
        if (isStudent) {
            scholarship = true;
            System.out.println("Student has a scholarship");
        } else {
            System.out.println("If he is not student - he can't has scolarship");
        }
    }

    public List<String> getSubjects() {
        List<String> subjects = new ArrayList<>();
        subjects.add("maths");
        subjects.add("english");
        subjects.add("history");
        return subjects;
    }

    public void summMethod(String s, int... args) {
        int summ = 0;
        int count = 0;
        for (int arg : args) {
            summ += arg;
            count++;
        }
        System.out.println(s + summ + "\n count is " + count);
    }
}
