package annotation;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.stream.Stream;

public class ObjectCognizer {
    ObjectCognizer(@org.jetbrains.annotations.NotNull Object object) {
        cognizeObjectClass(object);
    }

    public void cognizeObjectClass(Object object) {
        Class objectClass = object.getClass();
        String className = objectClass.getSimpleName();
        System.out.println("Class name of input object is: " + className);

        boolean anAbstract = Modifier.isAbstract(objectClass.getModifiers());
        if (anAbstract) {
            System.out.println(className + " is abstract class");
        } else {
            System.out.println(className + " is concrete class");
        }
        System.out.println("Class is in: " + objectClass.getPackage());

        Method[] declaredMethods = objectClass.getDeclaredMethods();
        System.out.println("Declared methods list: ");
        Stream.of(declaredMethods).forEach(System.out::println);

        Field[] declaredFields = objectClass.getDeclaredFields();
        System.out.println("Declared fields list: ");
        Stream.of(declaredFields).forEach(System.out::println);
    }

}
