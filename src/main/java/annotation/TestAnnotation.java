package annotation;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class TestAnnotation {

    public void printsFieldsWithAnnotation(Object object) {
        List<Field> fieldsWithAnnotation = new ArrayList<Field>();
        Field[] fields = object.getClass().getDeclaredFields();
        for (
                Field f : fields) {
            boolean fieldWithAnnotation = f.isAnnotationPresent(MyAnnotation.class);
            if (fieldWithAnnotation == true) {
                fieldsWithAnnotation.add(f);
            }
        }
        fieldsWithAnnotation.forEach(System.out::println);
    }

    public void printAnnotationValue(String field, Object object, String methodName)
            throws NoSuchFieldException, NoSuchMethodException {
        Field ageField = object.getClass().getDeclaredField(field);
        MyAnnotation myAnnotation = ageField.getAnnotation(MyAnnotation.class);
        Method method = object.getClass().getDeclaredMethod(methodName);
        AnnotationForMethod annotation = method.getAnnotation(AnnotationForMethod.class);
        System.out.println("@MyAnnotation value is: " + myAnnotation.age() + " and @AnnotationForMethod value is: " + annotation.value());
    }

    public void setValueDontKnowFieldType(Field field, Object object, String value) throws IllegalAccessException {
        field.setAccessible(true);
        if (field.getType() == boolean.class) {
            field.set(object, Boolean.parseBoolean(value));
        }
        if (field.getType() == int.class) {
            field.setInt(object, Integer.parseInt(value));
        }
        if (field.getType() == short.class) {
            field.set(object, Short.parseShort(value));
        }
        if (field.getType() == byte.class) {
            field.set(object, Byte.parseByte(value));
        }
        if (field.getType() == long.class) {
            field.setLong(object, Long.parseLong(value));
        }
        if (field.getType() == float.class) {
            field.setFloat(object, Float.parseFloat(value));
        }
        if (field.getType() == double.class) {
            field.setDouble(object, Double.parseDouble(value));
        } else {
            field.set(object, value);
        }
    }

    public void setValueYouKnowFieldType(Field field, Object object, String value, Type type) throws IllegalAccessException {
        field.setAccessible(true);
        field.set(object, value);
    }

    public void invokeHasScholarshipMethod(Object object) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method methodScholarship = object.getClass().getDeclaredMethod("hasScholarship", boolean.class, boolean.class);
        methodScholarship.invoke(object, false, true);
    }

    public void invokeGetSubjectsMethod(Object object) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method methodGetSubjects = object.getClass().getDeclaredMethod("getSubjects");
        System.out.println(methodGetSubjects.invoke(object));

    }

    public void invokeCalculateGraduationYearMethod(Object object) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method methodCalculateGraduation = object.getClass().getDeclaredMethod("calculateGraduationYear", int.class);
        Object o = methodCalculateGraduation.invoke(object, 25);
        System.out.println(o);
    }

    public void summMethodInvocation(Object object) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class[] parameterTypes = new Class[]{String.class, int[].class};
        Method summMethodtest = object.getClass().getDeclaredMethod("summMethod", parameterTypes);
        Object[] var2 = new Object[]{new String("my perfect string"),
                new Integer[]{10, 100}};

        summMethodtest.invoke(object, var2);
    }
}